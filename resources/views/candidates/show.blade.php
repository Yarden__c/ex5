@extends('layouts.app')
@section('content')
@section('title','Candidates')
@if(Session::has('notallowed'))
<div class='alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<body class='text-center'>

<h1>candidate</h1>
<table class="table">
    <!-- insert the header of the table-->
    <tr>
        <th>id</th><th>name</th><th>email</th><th>age</th><th>owner</th><th>status</th>
    </tr>
    <!-- insert the table data -->
        <tr>
        <td>{{$candidate->id}}</td>
        <td>{{$candidate->name}}</td>
        <td>{{$candidate->email}}</td>
        <td>{{$candidate->age}}</td>
        <td>{{$candidate->owner->name}}</td>
        <td>{{$candidate->status->name}}</td>
        </table>
        <form method = "POST" action = "{{route('candidates.changestatusfromcandidate')}}">

        @csrf  
                    <div class="form-group row">
                    <label for="status_id" class="col-md-4 col-form-label text-md-right">Move to status</label>
                    <div class="col-md-6">
                        <select class="form-control" name="status_id">                                                                         
                          @foreach (App\Status::next($candidate->status_id) as $status)
                          <option value="{{ $status->id }}"> 
                              {{ $status->name }} 
                          </option>
                          @endforeach    
                        </select>
                    </div>
                    <input name="id" type="hidden" value = {{$candidate->id}} >
                    <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Change status
                                </button>
                            </div>
                    </div>                    
                </form> 
                </div>
</body> 
@endsection