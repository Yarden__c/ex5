@extends('layouts.app')
@section('title','Create user')
@section('content')
<body class="text-center">
            <div class="content">
                <div class="title m-b-md">

                    <h1>Create user </h1>
                    <form method = "post" action = "{{action('UsersController@store')}}">
                    @csrf <!--אבטחת מידע-->

                    <div>
                        <label for = "name">User name </label>
                        <input type = "text" name = "name">
                    </div>
                    <div>
                        <label for = "email">User email </label>
                        <input type = "text" name = "email">
                    </div>
                    <div>
                        <label for = "age">Password </label>
                        <input type = "password" name = "password">
                    </div>
                    <div>
                        <input type = "submit" class="btn btn-outline-primary" name = "submit" value = "Create user">
                    </div>
                    </form>
                    </body>
                    @endsection