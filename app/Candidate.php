<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Candidate extends Model
{
    use Sortable;
    protected $fillable=['name','email','age'];
    
    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }
    public function status(){
        return $this->belongsTo('App\Status','status_id');}
    public $sortable = ['age'];
}